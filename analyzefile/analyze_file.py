#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       analyze_file.py
#       
#       Copyright 2012 Cristian <cristian@CszMachine>
#       
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

import sys
import os
import subprocess
import os.path
import shutil
import re
import tempfile
import time
import json
import base64

ALL_TYPE_READERS = {
					'application/msword':'catdoc',
					'application/vnd.ms-powerpoint':'catppt',
					'application/vnd.ms-excel':'xls2txt',
					'application/vnd.ms-office':'xls2txt',
					#'application/vnd.oasis.opendocument.text': 'odt2txt',
					#'application/vnd.oasis.opendocument.spreadsheet':'ods2txt',
					'application/vnd.ms-word-2007+':'dwdoc2txt',
					'application/vnd.ms-powerpoint-2007+':'dwppt2txt',
					'application/vnd.ms-excel-2007+':'dwxls2txt',
					'application/pdf':'pdftotext',
					'application/x-7z-compressed':'7za',
					'application/zip':'7za',
					'application/x-tar':'7za',
					'application/x-bzip2':'7za',
					'application/x-gzip':'7za',
					'application/octet-stream':'7za',
					'application/x-iso9660-image':'7za',
					'text/troff':'text',
					'text/x-pascal':'text',
					'text/x-java':'text',
					'text/html':'text',
					'text/text':'text',
					'unknow':'strings'
					}

OFFICE_MIME_TYPES = {'application/msword': 'Microsoft Office Word', 
				  'application/vnd.ms-word-2007+': 'Microsoft Word 2007+', 
				  'application/vnd.ms-powerpoint': 'Microsoft Office PowerPoint', 
				  'application/vnd.ms-powerpoint-2007+': 'Microsoft PowerPoint 2007+', 
				  'application/vnd.ms-excel': 'Microsoft Office Excel', 
				  'application/vnd.ms-excel-2007': 'Microsoft Excel 2007+', 
				  'application/vnd.ms-office': 'None'
				 }

SSDEEP_TYPE_READERS = {
					'application/msword':'catdoc',
					'application/vnd.ms-powerpoint':'catppt',
					'application/vnd.ms-excel':'xls2txt',
					'application/vnd.ms-office':'xls2txt',
					#'application/vnd.oasis.opendocument.text': 'odt2txt',
					#'application/vnd.oasis.opendocument.spreadsheet':'ods2txt',
					'application/vnd.ms-word-2007+':'dwdoc2txt',
					'application/vnd.ms-powerpoint-2007+':'dwppt2txt',
					'application/vnd.ms-excel-2007+':'dwxls2txt',
					'application/pdf':'pdftotext',
					'text/text':'text'
					}

#Be careful with whitespace
osName = os.uname()[0]
if osName == 'Linux':
	dwBinaryDir = '/opt/drainware/bin/'
	osBinaryDir = ''
	pathTrash = '/dev/null'
	osSeparator = '/'
	tmpDirectory = '/tmp'
else:
	dwBinaryDir = '/bin/'
	osBinaryDir = '/bin/'
	pathTrash = 'null'
	osSeparator = '/'
	
	tmpDirectory = os.environ['USERPROFILE']+ "\\AppData\\Local\\Temp"
	

binCatDoc = dwBinaryDir + 'catdoc '
binXls2Txt = dwBinaryDir + 'xls2txt '
binCatPpt = dwBinaryDir + 'catppt '
binOds2Txt = dwBinaryDir + 'ods2txt '
binOdt2Txt = dwBinaryDir + 'odt2txt '
binOdp2Txt = dwBinaryDir + 'odp2txt '
binPdfToText = dwBinaryDir + 'pdftotext '
bin7z = dwBinaryDir + '7za '
binSsDeep = dwBinaryDir + 'ssdeep '
binStrings = osBinaryDir + 'strings '
binDirName = osBinaryDir + 'dirname '
binBaseName = osBinaryDir + 'basename '
binFile = osBinaryDir + 'file '
binCat = osBinaryDir + 'cat '
binMd5Sum = osBinaryDir + 'md5sum '
binSha1Sum = osBinaryDir + 'sha1sum '


class Tools():
	def __init__(self):
		pass

	@staticmethod
	def checkExistDirFile(path_name):
		return os.path.exists(path_name)

	@staticmethod
	def getJsonFromFile(path_name):
		try:
			f = open(path_name, 'r')
			content = ''
			for line in f:
				content += line
		except Exception, e:
			#print "Exception - Tools.getJsonFromFile", e
			content = None
		return content

	@staticmethod
	def readFile(path_name):
		try:
			f = open(path_name, 'r')
			content = f.readlines()
			f.close()
			content = Tools.cleanFileText(content)
		except Exception, e:
			#print "Exception - Tools.readFile", e
			content = None
		return content
	
	@staticmethod
	def writeFile(content, path_name):
		f = open(path_name, 'w')
		f.writelines(content)
		f.close()
	
	@staticmethod
	def cleanFileText(content):
		result = ''
		for l in content:
			l = l.replace('\t',' ').replace('\n',' ').replace('\r',' ')
			result = result + l
		result = re.sub(' +',' ', result)
		return result
	
	@staticmethod
	def decodeBase64(base64_string):
		return base64.b64decode(base64_string)

	@staticmethod
	def encodeBase64(normal_string):
		return base64.b64encode(normal_string)

	@staticmethod
	def generateTempDirName():
		temp_dir_name = tempfile.mkdtemp()
		#tmp_dir_name = Tools.getDirFileName(temp_dir_name) + osSeparator + 'O_odw' + Tools.getBaseFileName(temp_dir_name) + 'pmtwdo_O'
		tmp_dir_name = tmpDirectory + osSeparator + 'O_odw' + Tools.getBaseFileName(temp_dir_name) + 'pmtwdo_O'
		Tools.removeDir(temp_dir_name)
		return tmp_dir_name

	@staticmethod
	def generateTempDirNameOld():
		tmp_dir_name = tempfile.mkdtemp()
		tmp_dir_name = Tools.getBaseFileName(tmp_dir_name)
		Tools.removeDir(tmp_dir_name)
		tmp_dir_name = 'O_odw' + tmp_dir_name + 'pmtwdo_O'
		return tmp_dir_name
	
	@staticmethod
	def getDirFileName(path_name):
		command = binDirName + Tools.quotesFile(path_name)
		out = os.popen(command)
		dir_name = out.read().replace('\n', '')
		out.close()
		return dir_name 
	
	@staticmethod
	def getBaseFileName(path_name):
		command = binBaseName + Tools.quotesFile(path_name)
		out = os.popen(command)
		base_name = out.read().replace('\n', '')
		out.close()
		return base_name 
	
	@staticmethod
	def getRelativeFileName(path_name):
		sep = re.compile('O_odwtmp......pmtwdo_O/')
		aux = sep.split(path_name)
		if len(aux) > 1:
			relative_name = aux[1]
		else:
			relative_name = path_name
		return relative_name
	
	@staticmethod
	def getOnlyFileName(path_name):
		base_name = Tools.getBaseFileName(path_name)
		return base_name
	
	@staticmethod
	def createDirectory(path_name):
		os.makedirs(path_name)
	
	@staticmethod
	def changeDirectory(path_name):
		os.chdir(path_name)
	
	@staticmethod
	def copyFile(src, dst):
		shutil.copy2(src, dst)
	
	@staticmethod
	def renameFile(old, new):
		try:
			while Tools.checkExistDirFile(new):
				time.sleep(0.00001)
			os.rename(old, new)
		except:
			pass
	
	@staticmethod
	def removeFile(path_name):
		try:
			os.remove(path_name)
		except Exception, e:
			#print "Exception - Tools.removeFile:", e
			pass
	
	@staticmethod
	def removeDir(path_name):
		try:
			shutil.rmtree(path_name, ignore_errors = True)
		except Exception, e:
			#print "Exception - Tools.removeDir:", e
			pass
	
	@staticmethod
	def quotesFile(file_name):
		return '\"' + file_name + '\"'
	
	@staticmethod
	def cleanFileNameSpaces(base_name):
		return base_name.replace(' ', '')
	
	@staticmethod
	def cleanMimeFile(result):
		return result.replace('\n','').split(': ')[1]

	@staticmethod
	def getReaderFromMimeType(mime_type):
		try:
			reader_file = ALL_TYPE_READERS[mime_type]
		except Exception, e:
			#print "Exception - Tools.getReaderFromMimeType:", e
			reader_file = ALL_TYPE_READERS['unknow']
		return reader_file

	@staticmethod
	def getFileList(fdir_name):
		flist = []
		for dirname, dirnames, filenames in os.walk(fdir_name):
			for filename in filenames:
				flist.append(os.path.join(dirname, filename))
		return flist		

	@staticmethod
	def get7zFiles(fdir_name):
		zlist = []
		for dirname, dirnames, filenames in os.walk(fdir_name):
			for filename in filenames:
				path_name = os.path.join(dirname, filename)
				reader = Tools.getReaderFromMimeType(FileMimeType(path_name).getFinalMimeType())
				if reader == '7za':
					zlist.append(path_name)	
		return zlist

	@staticmethod
	def getDirFileList(fdir_name):
		dflist = os.listdir(fdir_name)
		return dflist
	
	@staticmethod
	def checkCompressedFile(file_name):
		command = (bin7z + 't -p"" ' + Tools.quotesFile(file_name) + ' 2> ' + pathTrash)
		out = os.popen(command)
		result = out.read()
		out.close()
		evry_ok = len(re.findall('Everything is Ok', result))
		if evry_ok > 0:
			return 0
		else:
			pswd_error = len(re.findall('encrypted file', result))
			if pswd_error > 0:
				return -1
			else:
				return -2
	
	@staticmethod
	def extractOfficeFile(file_name):
		dst_file_name = Tools.quotesFile(Tools.getDirFileName(file_name) + osSeparator + Tools.getBaseFileName(file_name) + '.ofc')
		if Tools.checkCompressedFile(file_name) == 0:
			command = bin7z + 'x -p"" -o' + dst_file_name + ' ' + Tools.quotesFile(file_name) + ' 2> ' + pathTrash
			out = os.popen(command)
			out.close()
		else:
			print 'Error: File is locked'

	@staticmethod
	def extractCompressFile(file_name_list, encrypted_matches, recursive = False):
		for file_name in file_name_list:
			src_path_name = file_name
			dst_path_name = Tools.getDirFileName(file_name) + osSeparator + Tools.getBaseFileName(file_name) + '.tmp'
			chk_cprs_file = Tools.checkCompressedFile(file_name)
			rel_path_name = Tools.getRelativeFileName(file_name)
			if chk_cprs_file == 0:
				command = bin7z + 'x -p"" -o' + Tools.quotesFile(dst_path_name) + ' ' + Tools.quotesFile(src_path_name) + ' 2> ' + pathTrash
				out = os.popen(command)
				out.close()
				Tools.removeFile(src_path_name)
				Tools.renameFile(dst_path_name, src_path_name)
				if recursive:
					zfile_list = Tools.get7zFiles(src_path_name)
					if len(zfile_list) > 0:
						Tools.extractCompressFile(zfile_list, encrypted_matches, True)
			else:
				if chk_cprs_file == -1:
					encrypted_matches.append(['000000000000000000000000', 'block', 'high', 'encryted', rel_path_name])

	@staticmethod
	def getCygFilePath(file_name):
		file_name_aux = file_name.split(":\\")
		if len(file_name_aux) > 1: 
			disk_drive = file_name_aux[0].lower()
			file_path = file_name_aux[1].replace('\\', '/')
			cyg_file_path = '/cygdrive/' + disk_drive + '/' + file_path
		else:
			file_path = file_name_aux[0].replace('\\', '/')
			cyg_file_path = file_path
		return cyg_file_path


class FileMimeType():
	def __init__(self, file_name):
		self.srcFileName = file_name
		self.bkupFileName = file_name + '.bk'
		self.ofcFileName = file_name + '.ofc'
		self.aplicationRegex = re.compile('Name of Creating Application: (.*?), ')
		self.officeAptRegex = re.compile('Microsoft(\sOffice)? (.*?)(\s\d{4})?[,|\+]')
		self.fileInformation = {}

	def getOfficeMimeType(self, mime_type):
		try:
			if mime_type == 'application/zip' or mime_type == 'application/octet-stream':
				if Tools.checkCompressedFile(self.srcFileName) == 0:
					Tools.extractOfficeFile(self.srcFileName)
					ofc_dirfile_list = Tools.getDirFileList(self.ofcFileName)
					if '_rels' in ofc_dirfile_list:
						if 'word' in ofc_dirfile_list:
							mime_type = 'application/vnd.ms-word-2007+'
						elif 'ppt' in ofc_dirfile_list:
							mime_type = 'application/vnd.ms-powerpoint-2007+'
						elif 'xs' in ofc_dirfile_list:
							mime_type = 'application/vnd.ms-excel-2007+'
						elif 'xl' in ofc_dirfile_list:
							mime_type = 'application/vnd.ms-excel-2007+'
						else:
							mime_type = mime_type
					elif 'mimetype' in ofc_dirfile_list:
						path_name = self.ofcFileName + '/mimetype'
						mime_type = Tools.readFile(path_name)
					else:
						mime_type = mime_type
					Tools.removeDir(self.ofcFileName)
		except Exception, e:
			#print "Exception - FileMimeType.getOfficeMimeType", e
			pass
		return mime_type

	def getApplicationName(self):
		try:
			if Tools.checkExistDirFile(self.srcFileName):	
				command = binFile + Tools.quotesFile(self.srcFileName)
				out = os.popen(command)
				info = out.read()
				out.close()
				groups = self.aplicationRegex.findall(info)
				if len(groups) > 0:
					aplication_name = groups[0]
				else:
					groups = self.officeAptRegex.search(info)
					if groups:
						aplication_name = re.sub(',',  '', groups.group(0))
					else:
						aplication_name = 'unknow'
			else:
				print 'Error: ' + Tools.quotesFile(self.srcFileName) + ' does not exist'
				exit(-1)
		except Exception, e:
			#print "Exception - FileMimeType.getApplicationName", e
			aplication_name = 'unknow'
		return aplication_name

	def getMimeEncode(self):
		try:
			if Tools.checkExistDirFile(self.srcFileName):	
				command = binFile + '--mime-encoding ' + Tools.quotesFile(self.srcFileName)
				out = os.popen(command)
				mime_encode = Tools.cleanMimeFile(out.read())
				out.close()
			else:
				print 'Error: ' + Tools.quotesFile(self.srcFileName) + ' does not exist'
				exit(-1)
		except Exception, e:
			#print "Exception - FileMimeType.getMimeEncode", e
			mime_encode = 'unknow'
		return mime_encode
	
	def getMimeType(self):
		try:
			if Tools.checkExistDirFile(self.srcFileName):	
				command = binFile + '--mime-type ' + Tools.quotesFile(self.srcFileName)
				out = os.popen(command)
				mime_type = Tools.cleanMimeFile(out.read())
				out.close()
				if mime_type[:4] == 'text':
					mime_type = 'text/text'
			else:
				print 'Error: ' + Tools.quotesFile(self.srcFileName) + ' does not exist'
				exit(-1)
		except Exception, e:
			#print "Exception - FileMimeType.getMimeType", e
			mime_type = 'unknow'
		return mime_type

	def getFinalMimeType(self):
		try:
			if Tools.checkExistDirFile(self.srcFileName):	
				Tools.copyFile(self.srcFileName, self.bkupFileName)
				mime_type = self.getMimeType()
				if mime_type == 'application/msword':
					office_name = self.getApplicationName()
					for office_type in OFFICE_MIME_TYPES:
							if OFFICE_MIME_TYPES.get(office_type) == office_name:
								mime_type = office_type
				else:
					if mime_type in OFFICE_MIME_TYPES.keys():
						mime_encode = self.getMimeEncode()
						if mime_encode == 'binary':
							mime_type = mime_type + '-2007+'
				mime_type = self.getOfficeMimeType(mime_type)
				Tools.removeFile(self.srcFileName)
				Tools.renameFile(self.bkupFileName, self.srcFileName)
			else:
				print 'Error: ' + Tools.quotesFile(self.srcFileName) + ' does not exist'
				exit(-1)
		except Exception, e:
			#print "Exception - FileMimeType.getFinalMimeType", e
			mime_type = 'unknow'
		return mime_type

import re
class TransformFileToTxt():
	
	def __init__(self, file_name, reader_file):
		self.iniFileName = file_name
		self.cygFilePath = Tools.quotesFile(Tools.getCygFilePath(file_name))
		self.srcFileName = Tools.quotesFile(file_name)
		self.readerFile = reader_file
		self.txtFileName = Tools.quotesFile(file_name + '.txt')
		self.srcOfficeName = file_name
		self.txtOfficeName = file_name + '.txt'
		self.ofcFileName = file_name + '.ofc'
		self.errorFilename = Tools.quotesFile(file_name + '.err')

	def getTextPlainFileName(self):
		return self.txtOfficeName

	def getTextFromDocx(self):
		Tools.extractOfficeFile(self.srcOfficeName)
		word_document = self.ofcFileName + '/word/document.xml'
		text_tag = re.compile('<w:t\b[^>]*>(.*?)</w:t>')
		content = Tools.readFile(word_document)
		result = re.findall(text_tag, content)
		content = []
		for line in result:
			line = line + '\n'
			content.append(line)
		Tools.writeFile(content, self.txtOfficeName)


	def getTextFromPptx(self):
		Tools.extractOfficeFile(self.srcOfficeName)
		total_slide_document = len(Tools.getDirFileList(self.ofcFileName + '/ppt/slides'))
		slide_document = self.ofcFileName + '/ppt/fullslides.xml'
		
		if Tools.checkExistDirFile(self.ofcFileName + '/ppt/notesSlides'):
			for number in range(1,total_slide_document):
				each_slide_document = Tools.quotesFile(self.ofcFileName + '/ppt/slides/slide' + str(number) + '.xml')
				each_notes_document = Tools.quotesFile(self.ofcFileName + '/ppt/notesSlides/notesSlide' + str(number) + '.xml')
				command = binCat + each_slide_document + ' ' + each_notes_document + ' >> ' + Tools.quotesFile(slide_document)
				out = os.popen(command)
				out.close()
		else:
			for number in range(1,total_slide_document):
				each_slide_document = Tools.quotesFile(self.ofcFileName + '/ppt/slides/slide' + str(number) + '.xml')
				command = binCat + each_slide_document + ' >> ' + Tools.quotesFile(slide_document)
				out = os.popen(command)
				out.close()

		text_tag = re.compile('<a:t>(.*?)</a:t>')
		content = Tools.readFile(slide_document)
		result = re.findall(text_tag, content)
		content = []
		for line in result:
			line = line + '\n'
			content.append(line)
		Tools.writeFile(content, self.txtOfficeName)

	def getTextFromXlsx(self):
		Tools.extractOfficeFile(self.srcOfficeName)
		if Tools.checkExistDirFile(self.ofcFileName + '/xl'):
			sheet_document = self.ofcFileName + '/xl/sharedStrings.xml'
		if Tools.checkExistDirFile(self.ofcFileName + '/xs'):
			sheet_document = self.ofcFileName + '/xs/sharedStrings.xml'
		text_tag = re.compile('<t\b[^>]*>(.*?)</t>')
		content = Tools.readFile(sheet_document)
		result = re.findall(text_tag, content)
		content = []
		for line in result:
			line = line + '\n'
			content.append(line)
		Tools.writeFile(content, self.txtOfficeName)

	def getTextFromFile(self):
		if self.readerFile == 'catdoc':
			command = binCatDoc + self.srcFileName + ' > ' + self.txtFileName  + '; ' + binCat + self.txtFileName
			out = os.popen(command)
			result = out.read(100)
			out.close()
			if result == '':
				command = binXls2Txt + self.srcFileName + ' > ' + self.txtFileName + ' 2> ' + self.errorFilename + ' || ' + binCat + self.errorFilename
				out = os.popen(command)
				out.close()
		elif self.readerFile == 'catppt':
			command = binCatPpt + self.srcFileName + ' > ' + self.txtFileName + ' 2> ' + self.errorFilename
			out = os.popen(command)
			out.close()
		elif self.readerFile == 'xls2txt':
			command = binXls2Txt + self.srcFileName + ' > ' + self.txtFileName + ' 2> ' + self.errorFilename + ' || ' + binCat + self.errorFilename
			out = os.popen(command)
			result = out.read()
			out.close()
			if result.find('No Workbook found') >= 0:
				command = binCatPpt + self.srcFileName + ' > ' + self.txtFileName
				out = os.popen(command)
				out.close()
		elif self.readerFile == 'pdftotext':
			command = binPdfToText + self.srcFileName + ' ' + self.txtFileName
			#arg = ['/opt/drainware/bin/pdftotext', self.iniFileName, self.txtOfficeName]
			#print arg
			#subprocess.Popen(arg)
			out = os.popen(command)
			out.close()
		elif self.readerFile == 'odt2txt':
			command = binOdt2Txt + self.srcFileName + ' > ' + self.txtFileName
			out = os.popen(command)
			out.close()
		elif self.readerFile == 'ods2txt':
			command = binOds2Txt + self.srcFileName + ' > ' + self.txtFileName
			out = os.popen(command)
			out.close()
		elif self.readerFile == 'odp2txt':
			command = binOdp2Txt + self.srcFileName + ' > ' + self.txtFileName
			out = os.popen(command)
			out.close()
		elif self.readerFile == 'dwdoc2txt':
			self.getTextFromDocx()
		elif self.readerFile == 'dwppt2txt':
			self.getTextFromPptx()
		elif self.readerFile == 'dwxls2txt':
			self.getTextFromXlsx()
		elif self.readerFile == 'text':
			Tools.copyFile(self.iniFileName, self.txtOfficeName)
		else:
			command = binStrings + self.srcFileName + ' > ' + self.txtFileName
			out = os.popen(command)
			out.close()
		command = binStrings + '-1 ' + self.txtFileName + ' > ' + self.txtFileName + '.dw'
		out = os.popen(command)
		out.close()
		content = Tools.readFile(self.txtOfficeName + '.dw')
		content = Tools.cleanFileText(content)
		Tools.writeFile(content, self.txtOfficeName)

class HashesFile():
	
	def __init__(self, path_name):
		self.iniFilePath = path_name
		self.srcFilePath = Tools.quotesFile(path_name)
	
	def getSsDeepValue(self):
		command = binSsDeep + '-sb ' + self.srcFilePath
		out = os.popen(command)
		result = out.readlines()
		out.close()
		try:
			result = result[1].split(',')[0]
		except Exception, e:
			#print "Exception - HashesFile.getSsDeepValue", e
			result = 'Unknow'
		return result

	def checkSsdeepFile(self, hashFilePath):
		command = binSsDeep + '-sbm ' + Tools.quotesFile(hashFilePath) + ' ' + self.srcFilePath
		out = os.popen(command)
		results = out.readlines()
		out.close()
		try:
			final_result = list()
			for result in results:
				result = result.replace('\n', '').replace(')', ''). split('.hsh:')[1].split(' (')
				result[0] = result[0].replace('\x00', '')
				final_result.append(result)
		except Exception, e:
			#print "Exception - HashesFile.checkSsdeepFile", e
			final_result = [[Tools.getBaseFileName(self.iniFilePath), '0']]
		return final_result

	def getMd5Value(self):
		out = os.popen(binMd5Sum + Tools.getCygFilePath(self.srcFilePath))
		result = out.read().split(' ')
		out.close()
		result = result[0]
		return result

	def getSha1Value(self):
		out = os.popen(binSha1Sum + Tools.getCygFilePath(self.srcFilePath))
		result = out.read().split(' ')
		out.close()
		result = result[0]
		return result


class AnalyzeFile():
	def __init__(self, file_name, json_rules, md5sha1 = True, ssdeep = True, regex = True):
		self.jsonRules = json.JSONDecoder().decode(json_rules)
		self.fileName = file_name
		self.txtFileName = None
		self.hashFilePath = file_name + '.hsh'
		self.relativePath = Tools.getRelativeFileName(file_name)
		self.chkmd5sha1 = md5sha1
		self.chkssdeep = ssdeep
		self.chkregex = regex
		self.md5sha1 = list()
		self.ssdeeps = list()
		self.regexes = list()
		self.analyzeMatch = list()
		self.rulesAction = None
		self.rulesSeverity = None
		self.screenShotSeverity = None
		self.blockEncryptFiles = None
		self.returnValue = 0

	def infoAnalyzeFile(self):
		print 'FileName  :', self.fileName
		print 'Md5 Sha1  :', self.md5sha1
		print 'Ssdeeps   :', self.ssdeeps
		print 'Regexes   :', self.regexes
		print 'BEncrypt  :', self.blockEncryptFiles

	def getSubconceptRules(self):
		for json_rule in self.jsonRules['subconcepts']:
			self.regexes.append([json_rule['id'], json_rule['subconcept'], json_rule['policies'], 'subconcept', json_rule['verify']])
			#self.regexes.append([json_rule['id'], json_rule['action'], json_rule['severity'], json_rule['rule']])
		
	def getRegexRules(self):
		for json_rule in self.jsonRules['rules']:
			self.regexes.append([json_rule['id'], json_rule['rule'], json_rule['policies'], 'rule', json_rule['verify']])
			#self.regexes.append([json_rule['id'], json_rule['action'], json_rule['severity'], json_rule['rule']])

	def getMd5Sha1Rules(self):
		for hashes in self.jsonRules['files']:
			self.md5sha1.append([hashes['id'], hashes['md5'], hashes['sha1'], hashes['policies'], 'file'])
			#self.md5sha1.append([hashes['id'], hashes['action'], hashes['severity'], hashes['md5'], hashes['sha1']])
	
	def getSsDeepRules(self):
		ssdeepfile = open(self.hashFilePath, 'w')
		ssdeepfile.write('ssdeep,1.1--blocksize:hash:hash,filename\n')
		for hashes in self.jsonRules['files']:
			self.ssdeeps.append([hashes['id'], hashes['file'], hashes['ssdeep'], hashes['policies'], 'file'])
			#self.ssdeeps.append([hashes['id'], hashes['name'], hashes['action'], hashes['severity'], hashes['ssdeep']])
			ssdeepfile.write(hashes['ssdeep'] + ',"' + hashes['file'] + '"\n')
		ssdeepfile.close()
	
	def getRulesAction(self):
		self.rulesAction = self.jsonRules['action']
		
	def getRulesSeverity(self):
		self.rulesSeverity = self.jsonRules['severity']
	
	def getBlockEncryptFiles(self):
		self.blockEncryptFiles = self.jsonRules['block_encrypted']
		return self.blockEncryptFiles

	def getScreenshotSeverity(self):
		self.screenshotSeverity = self.jsonRules['screenshot_severity']
	
	def transformFileTxt(self):
		fmt = FileMimeType(self.fileName)
		aplication_name = fmt.getApplicationName()
		mime_encode = fmt.getMimeEncode()
		mime_type = fmt.getFinalMimeType()
		reader_file = Tools.getReaderFromMimeType(mime_type)
		ftxt = TransformFileToTxt(self.fileName, reader_file)
		ftxt.getTextFromFile()
		self.txtFileName = ftxt.getTextPlainFileName()
	
	def checkMd5Sha1(self):
		#print "Chekeando Md5 y Sha1..."
		hf = HashesFile(self.fileName)
		md5FileValue = hf.getMd5Value()
		sha1FileValue = hf.getSha1Value()
		for md5sha1 in self.md5sha1:
			if (md5FileValue == md5sha1[1]):
				if (sha1FileValue == md5sha1[2]):
					self.analyzeMatch.append([md5sha1[0], self.rulesAction, self.rulesSeverity, md5sha1[3], 'Md5 & Sha1', md5sha1[4]])
					#self.analyzeMatch.append([md5sha1[0], md5sha1[1], md5sha1[2], 'Md5 & Sha1', 'hash'])
					self.chkssdeep = False

	def checkSsdeep(self):
		#print "Chekeando SsDeep..."
		fhashes = HashesFile(self.txtFileName)
		ssdeepMatches = fhashes.checkSsdeepFile(self.hashFilePath)
		for ssdeepMatch in ssdeepMatches:
			if int(ssdeepMatch[1]) > 50:
				ssdeepFile = ssdeepMatch[0]
				ssdeepMatch = 'Similar en ' + ssdeepMatch[1] + '%'
				for ssdeep in self.ssdeeps:
					if ssdeep[1] == ssdeepFile:
						self.analyzeMatch.append([ssdeep[0], self.rulesAction, self.rulesSeverity, ssdeep[3], ssdeepMatch, ssdeep[4]])
						#self.analyzeMatch.append([ssdeep[0], ssdeep[2], ssdeep[3], ssdeepMatch, 'hash'])
						self.chkregex = False

	def checkRegexes(self):
		#print "Chekeando Regex..."
		for regexRule in self.regexes:
			try:
				matches_list = list()
				regex = re.compile(regexRule[1])
				content = Tools.readFile(self.txtFileName)
				matches = regex.findall(content)
				for match in matches:
					if match != '':
						matches_list.append(match)
				if len(matches_list) > 0:
					if regexRule[4]:
						matches_list = self.checkVerify(regexRule[4], matches_list)
					if len(matches_list) > 0:
						self.analyzeMatch.append([regexRule[0], self.rulesAction, self.rulesSeverity, regexRule[2], matches_list, regexRule[3]])
						#self.analyzeMatch.append([regexRule[0], regexRule[1], regexRule[2], matches_list, 'regex'])
			except Exception, e:
				print "AnalyzeFile.checkRegexes", e
				pass #~ print value, 'Malformed regular expression'

	def checkVerify(self, verify, matches):
		verify_pathname = Tools.getDirFileName(self.fileName) + '/verify.py'
		verify_file = open(verify_pathname, 'w')
		verify_file.write(Tools.decodeBase64(verify))
		verify_file.close()
		new_list = list()
		for element in matches:
			command = binPython + '"' + verify_pathname + '" ' + Tools.encodeBase64(element) + '; echo $?'
			out = os.popen(command)
			if out.readline().replace('\n', '') == '0':
				new_list.append(element)
			out.close()
		return new_list

	def makeAnalysis(self):
		self.getMd5Sha1Rules()
		self.getSsDeepRules()
		self.getRegexRules()
		self.getSubconceptRules()
		self.getRulesAction()
		self.getRulesSeverity()
		self.getScreenshotSeverity()
		self.checkMd5Sha1()
		if self.chkssdeep or self.chkregex:
			self.transformFileTxt()
		if self.chkssdeep:
			self.checkSsdeep()
		if self.chkregex:
			self.checkRegexes()
		
	def getMatches(self):
		if len(self.analyzeMatch) > 0:
			file_coincidence = {"FileName" : self.relativePath, "Coincidences" : []}
			for c in self.analyzeMatch:
				coincidence = {"Id":str(c[0]), "Action":c[1], "Severity":c[2], "Policies":c[3], "Matches":c[4], "Type":c[5]}
				file_coincidence["Coincidences"].append(coincidence)
			return file_coincidence
	
	def getReturnValue(self):
		severities = { None:0, "low": 1, "medium": 2, "high": 3}
		if self.rulesAction != 'block':
			if severities[self.rulesSeverity] < severities[self.screenshotSeverity]:
				self.returnValue = 1
			else:
				self.returnValue = 2
		else:
			if severities[self.rulesSeverity] < severities[self.screenshotSeverity]:
				self.returnValue = 3
			else:
				self.returnValue = 4
		return self.returnValue
	
	@staticmethod
	def getEncryptedMatches(encryted_matches):
		final_result = list()	
		for encrypted_match in encryted_matches:
			file_coincidence = {"FileName" : encrypted_match[4], "Coincidences" : []}
			coincidence = {"Id":str(encrypted_match[0]), "Action":encrypted_match[1], "Severity":encrypted_match[2], "Policies":["Advanced"], "Matches":encrypted_match[3],  "Type":"encrypted"}
			file_coincidence["Coincidences"].append(coincidence)
			final_result.append(file_coincidence)
		return final_result

def generateSsdeepValue(path_name):
	fmt = FileMimeType(path_name)
	mime_type = fmt.getFinalMimeType()
	reader_file = Tools.getReaderFromMimeType(mime_type)
	if reader_file in SSDEEP_TYPE_READERS.values():
		ftxt = TransformFileToTxt(path_name, reader_file)
		ftxt.getTextFromFile()
		fhashes = HashesFile(ftxt.getTextPlainFileName())
		ssdeep = fhashes.getSsDeepValue()
	else:
		ssdeep = 'Unknow'
	print ssdeep


def analyzeFile(path_name, json_rules):
	fmt = FileMimeType(path_name)
	mime_type = fmt.getFinalMimeType()
	reader_file = Tools.getReaderFromMimeType(mime_type)
	final_result = {"Results":[]}
	if reader_file == '7za':
		af = AnalyzeFile(path_name, json_rules, True, False, False)
		af.makeAnalysis()
		if af.getMatches():
			final_result["Results"].append(af.getMatches())
		else:
			encrypted_matches = list()
			Tools.extractCompressFile([path_name], encrypted_matches, True)
			if len(encrypted_matches) == 0:
				for path_file in Tools.getFileList(path_name):
					af = AnalyzeFile(path_file, json_rules)
					af.makeAnalysis()
					if af.getMatches():
						final_result["Results"].append(af.getMatches())
			else:
				encrypted_opcion = af.getBlockEncryptFiles()
				if encrypted_opcion:
					final_result["Results"] = AnalyzeFile.getEncryptedMatches(encrypted_matches)
				else:
					for path_file in Tools.getFileList(path_name):
						af = AnalyzeFile(path_file, json_rules)
						af.makeAnalysis()
						if af.getMatches():
							final_result["Results"].append(af.getMatches())
	else:
		af = AnalyzeFile(path_name, json_rules)
		af.makeAnalysis()
		if af.getMatches():
			final_result["Results"].append(af.getMatches())
	if len(final_result['Results']) == 0:
		final_result['Results']='Clean'
		print json.JSONEncoder().encode(final_result)
		return 0
	print json.JSONEncoder().encode(final_result)
	return af.getReturnValue()

def createTempBackUp(path_name):
	src_file = path_name
	temp_dir =  Tools.generateTempDirName()
	temp_file = temp_dir + osSeparator + Tools.getBaseFileName(src_file)
	Tools.createDirectory(temp_dir)
	Tools.copyFile(src_file, temp_file)
	return temp_file

def removeTempBackUp(path_name):
	temp_dir = Tools.getDirFileName(path_name)
	Tools.removeDir(temp_dir)

def helpMessage():
	help_message = '''
Usage: 
	analyze_file.py --options
Options:
	--ssdeep: Generate ssdeep value of File
		> analyze_file.py --ssdeep FilePath
	--analyze: Analyze File with Json Rules 
		> analyze_file.py --analyze FilePath JsonRules
'''
	print help_message

def main():
	argc = len(sys.argv)
	
	global binPython
	if osName == 'Linux':
		binPython = 'python2.6 '
	else:
		binPython = '"'+ Tools.getCygFilePath(Tools.getDirFileName(sys.argv[0]) + '\\bin\\python2.6') + '" '	
	
	if argc == 3:
		''' analyze_file.py --ssdeep FilePath '''
		option = sys.argv[1]
		src_file = sys.argv[2]
		if option == '--ssdeep':
			tmp_bk_file = createTempBackUp(src_file)
			generateSsdeepValue(tmp_bk_file)
			removeTempBackUp(tmp_bk_file)
		else:
			helpMessage()
		return_value = 0
	elif argc == 4:
		''' analyze_file.py --analyze FilePath JsonRulesPath '''
		option = sys.argv[1]
		src_file = sys.argv[2]
		json_rules_path = sys.argv[3]
		json_rules = Tools.getJsonFromFile(json_rules_path)
		if option == '--analyze':
			tmp_bk_file = createTempBackUp(src_file)
			return_value = analyzeFile(tmp_bk_file ,json_rules)
			removeTempBackUp(tmp_bk_file)
		else:
			helpMessage()
			return_value = 0
	else:
		helpMessage()
		return_value = 0
	sys.exit(return_value)
	return 0

if __name__ == '__main__':
	main()


