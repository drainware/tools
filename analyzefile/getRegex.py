#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       getRegex.py
#       
#       Copyright 2012 Cristian <cristian@CszMachine>
#       
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

import sys
import os
import json
import time

def getJsonFromFile(path_name):
	try:
		f = open(path_name, 'r')
		content = ''
		for line in f:
			content += line
	except Exception, e:
		#print "Exception - getJsonFromFile", e
		content = None
	return content

def transformToUniversal(normal_json):
	normal_json_dict = json.JSONDecoder().decode(normal_json)

	regexes_list = []
	for subconcept in normal_json_dict['subconcepts']:
		regex = {}
		regex['id'] = subconcept['id']
		regex['type'] = 'subconcept'
		regex['expresion'] = subconcept['subconcept']
		regex['action'] = normal_json_dict['action']
		regex['severity'] = normal_json_dict['severity']
		regex['policies'] = subconcept['policies']
		regexes_list.append(regex)

	for rules in normal_json_dict['rules']:
		regex = {}
		regex['id'] = rules['id']
		regex['type'] = 'rule'
		regex['expresion'] = rules['rule']
		regex['action'] = normal_json_dict['action']
		regex['severity'] = normal_json_dict['severity']
		regex['policies'] = subconcept['policies']
		regexes_list.append(regex)
	
	configuration_dict = {}
	configuration_dict['screenshot_severity'] = normal_json_dict['screenshot_severity']
	configuration_dict['regex'] = regexes_list
	universal_json_dict = {'conf':  configuration_dict}
	
	print json.JSONEncoder().encode(universal_json_dict)


def helpMessage():
	help_message = '''
Usage: 
	getRegex.py normalJson
'''
	print help_message

def main():
	argc = len(sys.argv)
	if argc == 2:
		normal_json= getJsonFromFile(sys.argv[1])
		transformToUniversal(normal_json)
	else:
		helpMessage()
	return 0

if __name__ == '__main__':
	main()



